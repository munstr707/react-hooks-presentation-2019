import React from "react";
import "./App.css";
import { TodoClass } from "./TodoClass";
import { TodoHooks } from "./TodoHooks";

/**
 * Application design
 *  Add todos, mark todos as completed, all is saved to localstorage for quick access and storing (useEffect hook)
 */

const App: React.FC = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1
          style={{
            margin: 0
          }}
        >
          Ultimate Todo App
        </h1>
      </header>
      <div id="todos-body">
        <TodoClass />
        <TodoHooks />
      </div>
    </div>
  );
};

export default App;

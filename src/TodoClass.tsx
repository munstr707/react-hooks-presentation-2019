import * as React from "react";

type Todo = { completed: boolean; description: string };

interface State {
  todos: Array<Todo>;
  newTodoDescription: string;
}

const testStartingTodos: Array<Todo> = [
  { description: "take out trash", completed: true },
  { description: "cook food", completed: false },
  { description: "play with cat", completed: false },
  { description: "wash cloths", completed: false }
];

/**
 * Problems with classes and react components
 *  - At odds with composition model
 *  - "this"
 *  - lifecycle methods
 *  - sharable component logic (lack of leads to unnecessary nesting)
 *  - when to use Class vs Function components?
 *  - compiler optimizations
 */
export class TodoClass extends React.Component<{}, State> {
  state: State = {
    todos: [],
    newTodoDescription: "Enter new todo"
  };

  componentDidMount() {
    const todos = localStorage.getItem("todo-class");
    if (todos) {
      this.setState({ todos: JSON.parse(todos) });
    } else {
      localStorage.setItem("todo-class", JSON.stringify(testStartingTodos));
      this.setState({ todos: testStartingTodos });
    }
  }

  handleUpdateTodoDescription = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    this.setState({ newTodoDescription: event.target.value });
  };

  handleAddTodo = () => {
    const newState = this.state.todos.concat({
      description: this.state.newTodoDescription,
      completed: false
    });
    this.setState({
      todos: newState
    });
    localStorage.setItem("todo-class", JSON.stringify(newState));
  };

  handleCompleteTodo = (description: string) => () => {
    const newState = this.state.todos.map(todo =>
      todo.description === description ? { ...todo, completed: true } : todo
    );
    this.setState({
      todos: newState
    });
    localStorage.setItem("todo-class", JSON.stringify(newState));
  };

  handleUndoTodo = (description: string) => () => {
    const newState = this.state.todos.map(todo =>
      todo.description === description ? { ...todo, completed: false } : todo
    );
    this.setState({
      todos: newState
    });
    localStorage.setItem("todo-class", JSON.stringify(newState));
  };

  render() {
    return (
      <div>
        <h2>class todos</h2>
        <input
          type="text"
          value={this.state.newTodoDescription}
          onChange={this.handleUpdateTodoDescription}
        />
        <button onClick={this.handleAddTodo} style={{ marginLeft: "10px" }}>
          add todo
        </button>
        <div className="todo-section">
          <div>
            <h3>Not Completed</h3>
            {this.state.todos
              .filter(todo => todo.completed === false)
              .map(todo => (
                <div key={todo.description}>
                  <input
                    type="checkbox"
                    onChange={this.handleCompleteTodo(todo.description)}
                  />
                  <span>{todo.description}</span>
                </div>
              ))}
          </div>
          <div>
            <h3>Completed</h3>
            {this.state.todos
              .filter(todo => todo.completed === true)
              .map(todo => (
                <div key={todo.description}>
                  <input
                    type="checkbox"
                    checked
                    onChange={this.handleUndoTodo(todo.description)}
                  />
                  <span>{todo.description}</span>
                </div>
              ))}
          </div>
        </div>
      </div>
    );
  }
}

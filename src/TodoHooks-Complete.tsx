import * as React from "react";

type Todo = { completed: boolean; description: string };

const testStartingTodos: Array<Todo> = [
  { description: "play video games", completed: true },
  { description: "eat cake", completed: false },
  { description: "play more video games", completed: false },
  { description: "eat more cake", completed: false }
];

const TodoHooks: React.FC = () => {
  const [todos, setTodos] = React.useState<Array<Todo>>([]);
  const [newTodoDescription, setNewTodoDescription] = React.useState(
    "Enter New Todo"
  );

  React.useEffect(() => {
    const todos = localStorage.getItem("todos-hook");
    if (todos) {
      setTodos(JSON.parse(todos));
    } else {
      localStorage.setItem("todos-hook", JSON.stringify(testStartingTodos));
      setTodos(testStartingTodos);
    }
  }, []);

  const handleUpdateTodoDescription = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setNewTodoDescription(event.target.value);
  };

  const handleAddTodo = () => {
    const newTodos = todos.concat({
      description: newTodoDescription,
      completed: false
    });
    setTodos(newTodos);
    localStorage.setItem("todos-hook", JSON.stringify(newTodos));
  };

  const handleCompleteTodo = (description: string) => () => {
    const newTodos = todos.map(todo =>
      todo.description === description ? { ...todo, completed: true } : todo
    );
    setTodos(newTodos);
    localStorage.setItem("todos-hook", JSON.stringify(newTodos));
  };

  const handleUndoTodo = (description: string) => () => {
    const newTodos = todos.map(todo =>
      todo.description === description ? { ...todo, completed: false } : todo
    );
    setTodos(newTodos);
    localStorage.setItem("todos-hook", JSON.stringify(newTodos));
  };

  return (
    <div>
      <h2>hooks todo</h2>
      <input
        type="text"
        value={newTodoDescription}
        onChange={handleUpdateTodoDescription}
      />
      <button onClick={handleAddTodo} style={{ marginLeft: "10px" }}>
        add todo
      </button>
      <div className="todo-section">
        <div>
          <h3>Not Completed</h3>
          {todos
            .filter(todo => todo.completed === false)
            .map(todo => (
              <div key={todo.description}>
                <input
                  type="checkbox"
                  onChange={handleCompleteTodo(todo.description)}
                />
                <span>{todo.description}</span>
              </div>
            ))}
        </div>
        <div>
          <h3>Completed</h3>
          {todos
            .filter(todo => todo.completed === true)
            .map(todo => (
              <div key={todo.description}>
                <input
                  type="checkbox"
                  checked
                  onChange={handleUndoTodo(todo.description)}
                />
                <span>{todo.description}</span>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

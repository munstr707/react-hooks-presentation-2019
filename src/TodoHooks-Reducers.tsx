import * as React from "react";

type Todo = { completed: boolean; description: string };

const testStartingTodos: Array<Todo> = [
  { description: "play video games", completed: true },
  { description: "eat cake", completed: false },
  { description: "play more video games", completed: false },
  { description: "eat more cake", completed: false }
];

interface State {
  todos: Array<Todo>;
  newTodoDescription: string;
}

export const todosReducer = (state: State, action: any): State => {
  switch (action.type) {
    case "LOAD_TODOS":
      return { ...state, todos: action.todos };
    case "ADD_TODO":
      return {
        ...state,
        todos: state.todos.concat({
          description: state.newTodoDescription,
          completed: false
        })
      };
    case "COMPLETE_TODO":
      return {
        ...state,
        todos: state.todos.map(todo =>
          todo.description === action.description
            ? { ...todo, completed: true }
            : todo
        )
      };
    case "UNDO_TODO":
      return {
        ...state,
        todos: state.todos.map(todo =>
          todo.description === action.description
            ? { ...todo, completed: false }
            : todo
        )
      };
    case "SET_NEW_DESCRIPTION":
      return { ...state, newTodoDescription: action.newTodoDescription };
    default:
      return state;
  }
};

const TodoHooksReducers: React.FC = () => {
  const [state, dispatch] = React.useReducer(todosReducer, {
    todos: [],
    newTodoDescription: ""
  });

  React.useEffect(() => {
    const todos = localStorage.getItem("todos-hook");
    if (todos) {
      dispatch({ type: "LOAD_TODOS", todos: JSON.parse(todos) });
    } else {
      localStorage.setItem("todos-hook", JSON.stringify(testStartingTodos));
      dispatch({ type: "LOAD_TODOS", todos: testStartingTodos });
    }
  }, []);

  const handleUpdateTodoDescription = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    dispatch({
      type: "SET_NEW_DESCRIPTION",
      newTodoDescription: event.target.value
    });
  };

  const handleAddTodo = () => {
    dispatch({ type: "ADD_TODO" });
  };

  const handleCompleteTodo = (description: string) => () => {
    dispatch({ type: "COMPLETE_TODO", description });
  };

  const handleUndoTodo = (description: string) => () => {
    dispatch({ type: "UNDO_TODO", description });
  };

  return (
    <div>
      <h2>hooks todo</h2>
      <input
        type="text"
        value={state.newTodoDescription}
        onChange={handleUpdateTodoDescription}
      />
      <button onClick={handleAddTodo} style={{ marginLeft: "10px" }}>
        add todo
      </button>
      <div className="todo-section">
        <div>
          <h3>Not Completed</h3>
          {state.todos
            .filter(todo => todo.completed === false)
            .map(todo => (
              <div key={todo.description}>
                <input
                  type="checkbox"
                  onChange={handleCompleteTodo(todo.description)}
                />
                <span>{todo.description}</span>
              </div>
            ))}
        </div>
        <div>
          <h3>Completed</h3>
          {state.todos
            .filter(todo => todo.completed === true)
            .map(todo => (
              <div key={todo.description}>
                <input
                  type="checkbox"
                  checked
                  onChange={handleUndoTodo(todo.description)}
                />
                <span>{todo.description}</span>
              </div>
            ))}
        </div>
      </div>
    </div>
  );
};

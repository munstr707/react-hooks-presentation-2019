import * as React from "react";

type Todo = { completed: boolean; description: string };

const testStartingTodos: Array<Todo> = [
  { description: "play video games", completed: true },
  { description: "eat cake", completed: false },
  { description: "play more video games", completed: false },
  { description: "eat more cake", completed: false }
];

export const TodoHooks: React.FC = () => {
  return (
    <div>
      <h2>hooks todo</h2>
    </div>
  );
};
